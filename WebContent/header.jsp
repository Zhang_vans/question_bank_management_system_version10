
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/bootstrap/css/default.css">

<!-- <link rel="stylesheet" href="${pageContext.request.contextPath}/css/carousel.css" > -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/style.css">

<!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
<script src="${pageContext.request.contextPath}/js/jquery-1.11.3.js"></script>
<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
<script
	src="${pageContext.request.contextPath}/css/bootstrap/js/bootstrap.min.js"></script>
<!-- <script src="${pageContext.request.contextPath}/js/common.js"></script> -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Neusoft题库管理系统</title>
<head>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
	$(".leftsidebar_box dt").css({
		"background-color" : "#3992d0"
	});
	$(function() {
		$(".leftsidebar_box dd").hide();
		$(".leftsidebar_box dt").click(function() {
			$(".leftsidebar_box dt").css({
				"background-color" : "#3992d0"
			})
			$(this).css({
				"background-color" : "#317eb4"
			});
			$(this).parent().find('dd').removeClass("menu_chioce");
			$(".menu_chioce").slideUp();
			$(this).parent().find('dd').slideToggle();
			$(this).parent().find('dd').addClass("menu_chioce");
		});
	})
</script>
<style type="text/css">
body {
	margin: 0;
	padding: 0;
	overflow-x: hidden;
}

html, body {
	height: 100%;
}

img {
	border: none;
}

* {
	font-family: '微软雅黑';
	font-size: 12px;
	color: #626262;
}

dl, dt, dd {
	display: block;
	margin: 0;
}

a {
	text-decoration: none;
}

#bg {
	background-image: url(images/content/dotted.png);
}

.containers {
	width: 100%;
	height: 100%;
	margin: auto;
}

/*left*/
.leftsidebar_box {
	width: 160px;
	height: auto !important;
	overflow: visible !important;
	position: fixed;
	height: 100% !important;
	background-color: #3992d0;
}

.line {
	height: 2px;
	width: 100%;
	background-image: url(images/left/line_bg.png);
	background-repeat: repeat-x;
}

.leftsidebar_box dt {
	padding-left: 40px;
	padding-right: 10px;
	background-repeat: no-repeat;
	background-position: 10px center;
	color: #f5f5f5;
	font-size: 14px;
	position: relative;
	line-height: 48px;
	cursor: pointer;
}

.leftsidebar_box dd {
	background-color: #317eb4;
	padding-left: 40px;
}

.leftsidebar_box dd a {
	color: #f5f5f5;
	line-height: 20px;
}

.leftsidebar_box dt img {
	position: absolute;
	right: 10px;
	top: 20px;
}

.system_log dt {
	background-image: url(images/left/system.png)
}

.custom dt {
	background-image: url(images/left/custom.png)
}

.channel dt {
	background-image: url(images/left/channel.png)
}

.app dt {
	background-image: url(images/left/app.png)
}

.cloud dt {
	background-image: url(images/left/cloud.png)
}

.syetem_management dt {
	background-image: url(images/left/syetem_management.png)
}

.source dt {
	background-image: url(images/left/source.png)
}

.statistics dt {
	background-image: url(images/left/statistics.png)
}

.leftsidebar_box dl dd:last-child {
	padding-bottom: 10px;
}

.divForm {
	position: absolute; /*绝对定位*/
	width: 300px;
	height: 200px;
	text-align: center; /*(让div中的内容居中)*/
	top: 50%;
	left: 50%;
	margin-top: -200px;
	margin-left: -150px;
}
</style>


<style>
.item {
	line-height: 100px;
	font-size: 50px;
	height: 100px;
	clear: both;
}

.item .text {
	display: inline-block;
	float: left;
}

.item:hover .close {
	display: inline-block;
}

.close {
	position: relative;
	display: none;
	width: 100px;
	height: 100px;
	overflow: hidden;
	border-radius: 50px;
	background: black;
	opacity: 0.5;
}

.close:hover {
	cursor: pointer;
	opacity: 1;
}

.close::before, .close::after {
	content: '';
	position: absolute;
	width: 80%;
	top: 50%;
	left: 10%;
	height: 12px;
	margin-top: -6px;
	border-radius: 5px;
	background: #ffffff;
}

.close::before {
	transform: rotate(45deg);
}

.close::after {
	transform: rotate(-45deg);
}
</style>

</head>

<body id="bg">

	<div class="containers">
		<div class="leftsidebar_box" id="background1">
			<div class="line"></div>
			<h1 style="color: #ffffff; font-size: 38px";>Neusoft</h1>

			<div align="center" style="margin-top: 60px">
				<img height="100" width="100"
					style="border-radius: 50%; text-align: center"
					src="${pageContext.request.contextPath}/userPhoto.do?userid=${user.userid}&photoPath=${user.photoPath}">
			</div>
			<div align="center" style="margin-bottom: 30px; margin-top: 30px">
				<a style="color: #ffffff;">用户：${user.username}</a>

			</div>


			<dl class="custom">
				<dt onClick="changeImage()">
					个人管理<img src="images/left/select_xl01.png">
				</dt>
				<dd class="first_dd">
					<a
						href="${pageContext.request.contextPath}/userlist.do?userid=${user.userid}">个人资料</a>
				</dd>
				<%
					int identity = (int) session.getAttribute("identity");
					if (identity == 1) {
				%>
				<dd>
					<a href="${pageContext.request.contextPath}/alluser.do">用户管理</a>
				</dd>
				<%
					}
					if (identity != 0 && identity != 1) {
				%>
				myFunction();
				<%
					response.sendRedirect("index.jsp");
					}
				%>

				<dd>
					<a
						href="${pageContext.request.contextPath}/resetPass.do?userid=${userid}">更改密码</a>
				</dd>
				<dd>
					<a href="index.jsp">退出登录</a>
				</dd>
				<dd>
					<a href="Register.jsp">用户注册</a>
				</dd>
			</dl>

			<dl class="source">
				<a href="${pageContext.request.contextPath}/ItemList.do"><dt >
					试题管理<img src="images/left/select_xl01.png">
				</dt></a>

			</dl>
			<script>
				function myFunction() {
					alert("您还没有登录，请登录");
				}
			</script>


			<dl class="syetem_management">
				<dt>
					关于我们<img src="images/left/select_xl01.png">
				</dt>
				<dd class="first_dd">
					<a href="#">大连东软信息学院</a>
				</dd>
				<dd>
					<a href="#">作者：张帆</a>
				</dd>
				<dd>
					<a href="#">电话：18041157978</a>
				</dd>
				<dd>
					<a href="#">邮箱：953920438@qq.com</a>
				</dd>
				<
			</dl>

		</div>
		<div style="margin-left: 160px">
			<div id="background2"
				style="background-color: #3992d0; height: 100px; width: 100%;">
				<p style="float: left; color: #ffffff; font-size: 50px">题库管理系统</p>
				<div style="float: right; padding: 18px">
					<ul>
						<li style="float: left; margin: 0 30px; display: inline;"><a
							href="${pageContext.request.contextPath}/ItemList.do"><img
								src="images/content/主页.png;">
								<h5 style="color: #ffffff; padding-left: 10px">主页</h5></a></li>
						<li style="float: left; margin: 0 30px; display: inline;"><a
							href="search.jsp"><img src="images/content/search.png;">
								<h5 style="color: #ffffff; padding-left: 10px">搜索</h5></a></li>
						<li style="float: left; margin: 0 30px; display: inline;"><a
							href="Setting.jsp"><img src="images/content/设置.png;">
								<h5 style="color: #ffffff; padding-left: 10px">设置</h5></a></li>
						<li style="float: left; margin: 0 30px; display: inline;"><a
							href="index.jsp"><img src="images/content/退出.png;">
								<h5 style="color: #ffffff; padding-left: 10px">退出</h5></a></li>

					</ul>

				</div>

			</div>