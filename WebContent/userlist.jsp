
<%@include file="/header.jsp" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <link rel="stylesheet"
	href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="http://cdn.static.runoob.com/libs/jquery/2.1.1/jquery.min.js"></script>
<script
	src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <ul class="breadcrumb">
	当前位置：
		<li>
			 <a href="${pageContext.request.contextPath}/ItemList.do">首页</a>
		</li>
		<li>
			 <a href="${pageContext.request.contextPath}/ItemList.do">个人管理</a>
		</li>
		<li class="active">
			个人资料
		</li>
	</ul>
<div style="margin-left:100px;margin-top:60px;margin-top:50px">

	<h3>个人资料</h3>
 <div style="margin-top:60px;width:40%">
    <table class="table table-bordered">
        <tr class="info">
            <th>学号</th>
            <th>${user.userid}</th>
        </tr >
        <tr>
            <th>姓名</th>
            <th>${user.username}</th>
        </tr>
        <tr>
            <th>学校</th>
            <th>${user.school}</th>
        </tr>
        <tr>
            <th>系别</th>
            <th>${user.department}</th>
        </tr>
        <tr>
            <th>班级</th>
            <th>${user.classid}</th>
        </tr>
        <tr>
            <th>年龄</th>
            <th>${user.userage}</th>
        </tr>
        <tr>
            <th>性别</th>
            <th>${user.usersex}</th>
        </tr>
        <tr>
            <th>邮箱</th>
            <th>${user.useremail}</th>
        </tr>
       <td><div data-toggle="modal" data-target="#myModal">
							<a class="btn btn-default">修改</a>
						</div>
						<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
							aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal"
											aria-hidden="true">&times;</button>
										<h4 class="modal-title" id="myModalLabel">修改个人资料</h4>
									</div>
									<div class="modal-body">
										<form role="form"
											action="${pageContext.request.contextPath}/userUpdate.do?userid=${user.userid}"
											method="post">
											<div class="form-group">
												<label for="name">学号（登录用户名）</label> ${user.userid}
											</div>
											<div class="form-group">
												<label for="name">姓名</label> <input type="text"
													class="form-control" id="username" name="username"
													value="${user.username}" placeholder="请输入用户名">
											</div>
											

											<div class="form-group">
												<label for="name">学校</label> <input type="text"
													class="form-control" id="school" name="school"
													value="${user.school}" placeholder="请输入学校">
											</div>

											<div class="form-group">
												<label for="name">系别</label> <input type="text"
													class="form-control" id="department" name="department"
													value="${user.department}" placeholder="请输入系别">
											</div>

											<div class="form-group">
												<label for="name">班级</label> <input type="text"
													class="form-control" id="classid" name="classid"
													value="${user.classid}" placeholder="请输入班级">
											</div>



											<div class="form-group">
												<label for="name">年龄</label> <input type="text"
													class="form-control" id="userage" name="userage"
													value="${user.userage}" placeholder="请输入年龄">
											</div>
											<div class="form-group">
												<label for="name">性别</label> <input type="text"
													class="form-control" id="usersex" name="usersex"
													value="${user.usersex}" placeholder="请输入性别">
											</div>
											<div class="form-group">
												<label for="name">邮箱</label> <input type="text"
													class="form-control" id="useremail" name="useremail"
													value="${user.useremail}" placeholder="请输入邮箱">
											</div>

											</center>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default"
											data-dismiss="modal">关闭</button>
										<button type="submit" class="btn btn-primary">提交更改</button>
										</form>
									</div>
								</div>
								<!-- /.modal-content -->
							</div>
							<!-- /.modal -->
						</div></td>
    </table>
    </div>
    </div>

</body>
</html>
