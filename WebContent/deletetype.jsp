
<%@include file="/header.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<link rel="stylesheet"
	href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="http://cdn.static.runoob.com/libs/jquery/2.1.1/jquery.min.js"></script>
<script
	src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<ul class="breadcrumb">
	当前位置：
	<li><a href="${pageContext.request.contextPath}/ItemList.do">首页</a>
	</li>
	<li><a href="${pageContext.request.contextPath}/ItemList.do">系别</a>
	</li>
	<li><a href="${pageContext.request.contextPath}/ItemList.do">科目</a>
	</li>
	<li class="active">删除科目</li>
</ul>	
	<div class="row">
	<c:forEach items="${subjects}" var="subject">
		<div class="col-sm-6 col-md-3">
			<a href="${pageContext.request.contextPath}/showFileAll.do?classid=${subject.classid}&typeid=${subject.typeid}"
				class="thumbnail"> <img src="subject.png" alt="通用的占位符缩略图">
				<center>
					<div class="caption">
						<h4>${subject.subjectName}</h4>
					</div>
					
				</center>
				
			</a>
			<div id="myAlert" style="float:right">
							<button class="btn btn-default">
								<a  href="${pageContext.request.contextPath}/typeDelete.do?classid=${subject.classid}&typeid=${subject.typeid}">删除</a>
							</button>
						</div>
		</div>
	</c:forEach>
	
		
</div>
	


</body>
<script>
            var ck = function(){
                alert("确定删除吗？");
            }
 </script>
</html>
