<%@include file="/header.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<link rel="stylesheet"
	href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="http://cdn.static.runoob.com/libs/jquery/2.1.1/jquery.min.js"></script>
<script
	src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<ul class="breadcrumb">
	当前位置：
	<li><a href="ItemList.do">首页</a></li>
	<li><a href="ItemList.do">试题库管理</a></li>
	<li class="active">试题导入</li>
</ul>
<div style="margin-left: 10%;width:50%;margin-top:50px">

           <%if(identity == 1){//用户没有登录
               %>
	<form action="${pageContext.request.contextPath}/upLoadFile.do" method="post" enctype="multipart/form-data">
		
		<input  type="file" name="photo"><br>
		
		
		<div class="form-group">
			<label for="name">作者</label> <input type="text" class="form-control"
				id="author" name="author" placeholder="请输入作者">
		</div>
		<div class="hide">
			<label for="name">系别号</label>   
			<input type="text" class="form-control"id="classid" name="classid" value="${classid}" placeholder="${classid}">
		</div>
		
		<div class="hide">
			<label for="name">typeid</label>   
			<input type="text" class="form-control"id="typeid" name="typeid" value="${typeid}" placeholder="${typeid}">
		</div>
				
		 <input class="btn btn-primary" type="submit" value="上传">
	</form>
	<%}%>
	</br> </br>	
	<table class="table table-bordered">
		<tr>
			<th>题目编号</th>
			<th>题目</th>
			<th>作者</th>
			<th>上传日期</th>
			<th>操作</th>
		</tr>

		<c:forEach items="${filealls}" var="file">
			<tr class="info">
				<td>${file.itemid}</td>
				<td>${file.fileName} </td>
				<td>${file.author}</td>
				<td>${file.pubdate}</td>
				<td>
				<a href="${pageContext.request.contextPath}/fileDownload.do?itemid=${file.itemid}">下载</a>
				<%if(identity == 1){//用户没有登录
               %>
                <a  onclick="ck();" href="${pageContext.request.contextPath}/itemDelete.do?itemid=${file.itemid}&classid=${file.classid}&typeid=${file.typeid}">删除</a></td>
                <%}%>
			</tr>
		</c:forEach>
	</table>
<script>
            var ck = function(){
                alert("确定删除吗？");
            }
 </script>
</div>
</body>
</html>