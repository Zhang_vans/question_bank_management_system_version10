<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
  <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html> 
<head> 
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap/css/default.css">

<!-- <link rel="stylesheet" href="${pageContext.request.contextPath}/css/carousel.css" > -->
 <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" >

<!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
<script src="${pageContext.request.contextPath}/js/jquery-1.11.3.js"></script>
<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
<script	src="${pageContext.request.contextPath}/css/bootstrap/js/bootstrap.min.js"></script>
<!-- <script src="${pageContext.request.contextPath}/js/common.js"></script> -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<title>用户注册</title> 
<script type="text/javascript"> 
</script> 
<style>
.divForm{
position: absolute;/*绝对定位*/
width: 300px;
height: 200px;
text-align: center;/*(让div中的内容居中)*/
top: 50%;
left: 50%;
margin-top: -200px;
margin-left: -150px;

}
</style>
</head> 
<body style="background-color:#3992d0;"> 
  <center><h1 class="jumbotron">您好欢迎使用本题库管理系统</h1> </center>
 
<div class="divForm">
  <h3>注册表</h3>
 
  <form role="form" action="${pageContext.request.contextPath }/RegisterView.do?identity=1"  method="post"  enctype="multipart/form-data">
      
      <div class="form-group">
		<label for="name">姓名</label>
		<input type="text" class="form-control" id="username" name="username" 
			   placeholder="请输入用户名">
	</div>
	
	
	<div class="form-group">
		<label for="name">学号（登录用户名）</label>
		<input type="text" class="form-control" id="userid" name="userid" 
			   placeholder="请输入学号">
	</div>
	
	<div class="hide">
		<label for="name">学号（登录用户名）</label>
		<input type="text" class="form-control" id="identity" name="identity" value="1"
			   placeholder="请输入学号">
	</div>
	
	<div class="form-group">
		<label for="name">密码</label>
		<input type="text" class="form-control" id="userpass" name="userpass" 
			   placeholder="请输入密码">
	</div>
	
	<div class="form-group">
		<label for="name">学校</label>
		<input type="text" class="form-control" id="school" name="school" 
			   placeholder="请输入学校">
	</div>
	
	<div class="form-group">
		<label for="name">系别</label>
		<input type="text" class="form-control" id="department" name="department" 
			   placeholder="请输入系别">
	</div>
	
	<div class="form-group">
		<label for="name">班级</label>
		<input type="text" class="form-control" id="classid" name="classid" 
			   placeholder="请输入班级">
	</div>
	
	
	
	<div class="form-group">
		<label for="name">年龄</label>
		<input type="text" class="form-control" id="userage" name="userage" 
			   placeholder="请输入年龄">
	</div>
	<div class="form-group">
		<label for="name">性别</label>
		<input type="text" class="form-control" id="usersex" name="usersex" 
			   placeholder="请输入性别">
	</div>
	<div class="form-group">
		<label for="name">邮箱</label>
		<input type="text" class="form-control" id="useremail" name="useremail" 
			   placeholder="请输入邮箱">
	</div>
	
	<div class="form-group">
		<label for="inputfile">上传头像 </label>
		<input type="file" id="photo" name="photo">
		<p class="help-block">请上传你的头像</p>
	</div>
	<button type="submit" class="btn btn-default">注册</button>
  </form>
 </div>
  
  
</body> 
</html> 