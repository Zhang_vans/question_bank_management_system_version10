<%@include file="/header.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<link rel="stylesheet"
	href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="http://cdn.static.runoob.com/libs/jquery/2.1.1/jquery.min.js"></script>
<script
	src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<ul class="breadcrumb">
	当前位置：
	<li><a href="${pageContext.request.contextPath}/ItemList.do">首页</a>
	</li>
	<li><a href="${pageContext.request.contextPath}/ItemList.do">系别</a>
	</li>
	<li class="active">科目</li>
</ul>
<div class="row">
	<c:forEach items="${subjects}" var="subject">
		<div class="col-sm-6 col-md-3">
			<a href="${pageContext.request.contextPath}/showFileAll.do?classid=${subject.classid}&typeid=${subject.typeid}"
				class="thumbnail"> <img src="subject.png" alt="通用的占位符缩略图">
				<center>
					<div class="caption">
						<h4>${subject.subjectName}</h4>
					</div>
				</center>
			</a>
		</div>
	</c:forEach>
<%
           if(identity == 1){//用户没有登录
               %>
	<div class="col-sm-6 col-md-3" data-toggle="modal"data-target="#myModal">
		<a class="thumbnail"> <img src="subjectadd.png" alt="通用的占位符缩略图">
			<center>
				<div class="caption">
					<h4>添加科目</h4>
				</div>
			</center>
		</a>
	</div>


	<div class="col-sm-6 col-md-3" data-toggle="modal"
		data-target="#delete">
		<a href="${pageContext.request.contextPath}/deletetype.do?classid=${classid}"
			class="thumbnail"> <img src="subjectdelete.png" alt="通用的占位符缩略图">
			<center>
				<div class="caption">
					<h4>删除科目</h4>
				</div>
			</center>
		</a>
	</div>
	<%}%>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">添加科目</h4>
				</div>
				<div class="modal-body">
					<form role="form"
						action="${pageContext.request.contextPath }/addtype.do?classid=${classid}"
						method="post">
						<div class="form-group">
							<label for="name">科目名</label> <input type="text"
								class="form-control" id="subjectName" name="subjectName"
								placeholder="请输入科目名">
						</div>

						</center>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭
					</button>
					<button type="submit" class="btn btn-primary">提交更改</button>
					</form>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
</div>
</div>
</div>
</body>
</html>
