
<%@include file="/header.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<link rel="stylesheet"
	href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="http://cdn.static.runoob.com/libs/jquery/2.1.1/jquery.min.js"></script>
<script
	src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<ul class="breadcrumb">
	当前位置：
	<li><a href="${pageContext.request.contextPath}/ItemList.do">首页</a>
	</li>
	<li><a href="${pageContext.request.contextPath}/ItemList.do">个人管理</a>
	</li>
	<li class="active">用户管理</li>
</ul>
<div style="margin-left: 100px; margin-top: 60px;">

	<h3>用户管理</h3>
	<div style="margin-top: 60px; width: 80%">
		<table class="table table-striped">
			<tr>
				<th>学号</th>
				<th>姓名</th>
				<th>年龄</th>
				<th>学校</th>
				<th>系别</th>
				<th>班级</th>
				<th>性别</th>
				<th>邮箱</th>
				<th>头像</th>
				<th>操作</th>
				<th></th>

			</tr>
			<c:forEach items="${users}" var="user">
				<tr>
					<td>${user.userid}</td>
					<td>${user.username}</td>
					<td>${user.userage}</td>
					<td>${user.school}</td>
					<td>${user.department}</td>
					<td>${user.classid}</td>
					<td>${user.usersex}</td>
					<td>${user.useremail}</td>
					<td><img height="100" width="100"
						style="border-radius: 50%; text-align: center"
						src="${pageContext.request.contextPath}/userPhoto.do?userid=${user.userid}&photoPath=${user.photoPath}">
					</td>
					
					<td><a onclick="ck();" class="btn btn-default"
						href="${pageContext.request.contextPath}/userDelete.do?userid=${user.userid}">删除</a></td>
				</tr>
			</c:forEach>

		</table>
	</div>

</div>
<script>
	var ck = function() {
		alert("确定删除吗？");
	}
</script>
</body>
</html>
