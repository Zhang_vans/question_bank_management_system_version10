
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Neusoft题库管理系统</title>
<style>
html{   
    width: 100%;   
    height: 100%;   
    overflow: hidden;   
    font-style: sans-serif;   
}   
body{   
    width: 100%;   
    height: 100%;   
    font-family: 'Open Sans',sans-serif;   
    margin: 0;  
    background-image: url(bac.jpg); 
    /*background-color: #1d86ae  ; */  //#4A374A
}   
#login{   
    position: absolute;   
    top: 50%;   
    left:50%;   
    margin: -150px 0 0 -150px;   
    width: 300px;   
    height: 300px;   
}   
#login h1{   
    color: #fff;   
    text-shadow:0 0 10px;   
    letter-spacing: 1px;   
    text-align: center;   
}   
h1{   
    font-size: 3em;   
    margin: 0.67em 0;   
    color: #fff;   
    margin-bottom: 30px;
}   
input{   
    width: 278px;   
    height: 18px;   
    margin-bottom: 10px;   
    outline: none;   
    padding: 10px;   
    font-size: 13px;   
    color: #fff;   
    text-shadow:1px 1px 1px;   
    border-top: 1px solid #312E3D;   
    border-left: 1px solid #312E3D;   
    border-right: 1px solid #312E3D;   
    border-bottom: 1px solid #56536A;   
    border-radius: 4px;   
    background-color: #2D2D3F;   
}   
.but{   
    width: 300px;   
    min-height: 20px;   
    display: block;   
    background-color: #1d86ae ;   
    border: 1px solid #3762bc;   
    color: #fff;   
    padding: 9px 14px;   
    font-size: 15px;   
    line-height: normal;   
    border-radius: 5px;   
    margin: 0; 
    margin-top:20px;  
} 
.divForm{
position: absolute;/*绝对定位*/
width: 300px;
height: 200px;
text-align: center;/*(让div中的内容居中)*/
top: 50%;
left: 50%;
margin-top: -200px;
margin-left: -150px;
}
</style>
</head>
<body>
	<div class="divForm">
	<h1 >Neusoft<br>题库管理系统</h1> 
	<form action="${pageContext.request.contextPath }/login.do" method="post">
    <input type="text" name="userid" id="userid" placeholder="用户名"><br>
	<input type="password" name="userpass" id="userpass" placeholder="密码"><br>
    <button class="but" type="submit">登录</button> 
</form>
<form action="Register.jsp" method="post">
<button class="but" type="submit">注册</button> 
</form>
<form action="MRegister.jsp" method="post">
<button class="but" type="submit">管理员注册</button> 
</form>
</div>
</body>
</html>