<%@include file="/header.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<link rel="stylesheet"
	href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="http://cdn.static.runoob.com/libs/jquery/2.1.1/jquery.min.js"></script>
<script
	src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<ul class="breadcrumb">
	当前位置：
	<li><a href="ItemList.do">首页</a></li>
	<li><a href="ItemList.do">试题库管理</a></li>
	<li class="active">搜索</li>
</ul>

<div style="margin-left: 10%; width: 50%; margin-top: 100px">
	<form action="${pageContext.request.contextPath}/search.do" method="post">
		<div class="form-group">
		<h2>请输入查询的题目：</h2>
			<input type="text" class="form-control"
				id="fileName" name="fileName" placeholder="请输入...">
				<input class="btn btn-primary" type="submit" value="搜索">
		</div>
	</form>
	
	
	<table class="table table-bordered">
		<tr>
			<th>题目编号</th>
			<th>题目</th>
			<th>作者</th>
			<th>上传日期</th>
			<th>操作</th>
		</tr>

		<c:forEach items="${filealls}" var="file">
			<tr class="info">
				<td>${file.itemid}</td>
				<td>${file.fileName} </td>
				<td>${file.author}</td>
				<td>${file.pubdate}</td>
				<td>
				<a href="${pageContext.request.contextPath}/fileDownload.do?path=${file.filePath}">下载</a>
			</tr>
		</c:forEach>
	</table>

</div>
</body>
</html>