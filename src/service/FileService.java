package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import entity.FileAll;
import mapper.FileAllMapper;

@Service
public class FileAllService {
	@Autowired
	FileAllMapper fileallmapper;	
	public List<FileAll> getFile(int classid, int typeid){
		System.out.println(classid);
		System.out.println(typeid);
		return fileallmapper.getFile(classid,typeid);
	}
	public boolean addFile(FileAll fileall) {
		// TODO Auto-generated method stub
		fileallmapper.addFile(fileall);
		return true;		
	}
	public boolean delete(String itemid) {
		
		// TODO Auto-generated method stub
		return fileallmapper.delete(itemid)>0;
	}
	public List<FileAll> search(String fileName) {
		// TODO Auto-generated method stub
		return fileallmapper.search(fileName);
	}
	public FileAll getdown(int itemid) {
		// TODO Auto-generated method stub
		return fileallmapper.getdown(itemid);
	}
}
