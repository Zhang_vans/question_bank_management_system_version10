package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import entity.User;
//import mapper.DBUtil;
//import mapper.UserDao;;
import mapper.UserMapper;

@Service
public class UserService {
	@Autowired
    public UserMapper userMapper;
    /**
     * 登录
     * 根据用户名和密码进行查询
     */
    public User login(String username, String userpass) {
	  System.out.println(username+"++++"+userpass);
        return userMapper.findByUserNameAndPassword(username, userpass);
    }
    /**
     * 注册
     * 增加用户
     */
    public void register(User user) {
        userMapper.addUser(user);    
    }
    /**
     * 根据用户名查询
     */
    public User findByUserName(String username) {
        return userMapper.findByUserName(username);
    }
	public List<User> getUsers() {
		// TODO Auto-generated method stub
		return userMapper.getUsers();
	}
	public boolean delete(String userid) {
		// TODO Auto-generated method stub
		return userMapper.delete(userid)>0;
	}
	public boolean updateUser(User user) {
		// TODO Auto-generated method stub
		return userMapper.updateUser(user)>0;
	}
	public User findByUserid(String userid) {
		// TODO Auto-generated method stub
		return userMapper.findByUserid(userid);
	}
	public boolean updatePass(String userid, String userpass) {
		// TODO Auto-generated method stub
		return userMapper.updatePass(userid,userpass)>0;
	}
	
}
