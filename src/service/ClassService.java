package service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import entity.ClassName;
import entity.FileAll;
import mapper.ClassNameMapper;
import mapper.FileAllMapper;
@Service
public class FileService {
	
	@Autowired
	ClassNameMapper classnameMapper;
	
	public  List<ClassName> className() {
		// TODO Auto-generated method stub
		return classnameMapper.className();
	}
	
	public boolean addclass(ClassName className){
		classnameMapper.addclass(className);
		return true;
	}
	public boolean delete(int classid) {
		return classnameMapper.delete(classid)>0;
	}
	
	
}
