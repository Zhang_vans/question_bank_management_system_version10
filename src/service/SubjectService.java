package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import entity.Subject;
import mapper.SubjectMapper;

@Service
public class SubjectService {
	@Autowired
	SubjectMapper subjectmapper;

	public List<Subject> getSubject(int classid) {
		// TODO Auto-generated method stub
		return subjectmapper.getSubject(classid);
	}
	public boolean addtype(Subject subject) {
		// TODO Auto-generated method stub
		subjectmapper.addtype(subject);
		return true;
	}
	public List<Subject> subjectName(int classid, int typeid) {
		// TODO Auto-generated method stub
		return subjectmapper.subjectName(classid,typeid);
	}

	public boolean delete(int classid, int typeid) {
		// TODO Auto-generated method stub
		return subjectmapper.delete(classid,typeid)>0;
	}
	

}
