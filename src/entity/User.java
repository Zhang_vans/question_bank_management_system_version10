package entity;

public class User { 

	private String userid;
	private String userage;
	private String username;
	private String userpass;
	private String usersex;
	private String useremail;
	private String photoPath;
	private String filePath;
	private String school;
	private String department;
	private String classid;
	int identity;
	public String getSchool() {
		return school;
	}

	public int getIdentity() {
		return identity;
	}

	public void setIdentity(int identity) {
		this.identity = identity;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getClassid() {
		return classid;
	}

	public void setClassid(String classid) {
		this.classid = classid;
	}

	
	
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getPhotoPath() {
		return photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}

	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public User(String userId, String userAge, String userName, 
			String userPass, String userSex, String userEmail
		) {
		super();
		this.userid = userId;
		this.userage = userAge;
		this.username = userName;
		this.userpass = userPass;
		this.usersex = userSex;
		this.useremail = userEmail;

	}

	

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getUserage() {
		return userage;
	}

	public void setUserage(String userage) {
		this.userage = userage;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserpass() {
		return userpass;
	}

	public void setUserpass(String userpass) {
		this.userpass = userpass;
	}

	public String getUsersex() {
		return usersex;
	}

	public void setUsersex(String usersex) {
		this.usersex = usersex;
	}

	public String getUseremail() {
		return useremail;
	}

	public void setUseremail(String useremail) {
		this.useremail = useremail;
	}

	@Override
	public String toString() {
		return "User [userId=" + userid + ", userAge=" + userage + ", userName=" + username + ", userPass=" + userpass
				+ ", userSex=" + usersex + ", userEmail=" + useremail + ", userRank="  + "]";
	}

	
}
