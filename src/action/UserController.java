package action;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.stereotype.Controller;

import entity.User;
import service.UserService;

@Controller
@Scope("prototype")
public class UserController {
    @Autowired
     UserService userService;
    
    @RequestMapping("/resetPass.do")
    public ModelAndView resetPass(String userid){
    	Map<String,Object> model = new HashMap<String,Object>();
		model.put("userid",userid);
		return new ModelAndView("resetPass",model);
    }
    
    @RequestMapping("/resetPassdo.do")
    public ModelAndView resetPassdo(String userid,String userpass){
    	Map<String,Object> model = new HashMap<String,Object>();
    	boolean ok = userService.updatePass(userid,userpass);
		if (ok) {
			return new ModelAndView("index", model);
		} else
			return new ModelAndView("Error", model);
	}
	  
    @RequestMapping("/alluser.do")
    public ModelAndView alluser(){
    	Map<String,Object> model = new HashMap<String,Object>();
		List<User> users = userService.getUsers();
		model.put("users",users);
		return new ModelAndView("userManage",model);
    }
    
    @RequestMapping("/userDelete.do")
	public ModelAndView delete(String userid){
		boolean ok = userService.delete(userid);
		System.out.println(userid);
		Map<String,Object> model = new HashMap<String,Object>();
		List<User> users = userService.getUsers();
		model.put("users",users);
		return new ModelAndView("userManage",model);
	}
    
    
    @RequestMapping("/userUpdate.do")
	public ModelAndView updateView(String userid,User user){
    	System.out.println("更新"+userid);
		boolean ok = userService.updateUser(user);
		Map<String,Object> model = new HashMap<String,Object>();
		User users = userService.findByUserid(userid);
		model.put("users",users);
		return new ModelAndView("userlist",model);
	}
    
    @RequestMapping("/userlist.do")
	public ModelAndView userlist(String userid){
		Map<String,Object> model = new HashMap<String,Object>();
		User users = userService.findByUserid(userid);
		model.put("users",users);
		return new ModelAndView("userlist",model);
	}
//    @RequestMapping("/userlist.do")
//    public String userlistpage() {
//        return "user/userlist";
//    }
//    @RequestMapping("/userlist.do")
//	public ModelAndView studentList(){
//		Map<String,Object> model = new HashMap<String,Object>();
//		List<User> user = userService.getUsers();
//		System.out.println(user.toString());
//		model.put("user",user);
//		return new ModelAndView("userlist",model);
//	}
    
    /*
	 * 显示照片
	 */
	@RequestMapping("/userPhoto.do")
	public void studentPhoto(String userid,String photoPath,HttpServletRequest request,HttpServletResponse response){
        // 找到文件
		System.out.println("photo");
		ServletContext application = request.getServletContext();
		String realPath = application.getRealPath("/photo");
		String fileName = realPath+File.separator+userid+"."+photoPath;
		System.out.println(fileName);
        File file = new File(fileName);
        if (file.exists()) {
        	byte[] buffer = new byte[1024];
        	FileInputStream fis = null;
        	BufferedInputStream bis = null;
        	try {
        		System.out.println("图片");
        		fis = new FileInputStream(file);
                bis = new BufferedInputStream(fis);
                OutputStream os = response.getOutputStream();
                int i = bis.read(buffer);
                while (i != -1) {
                	os.write(buffer, 0, i);
                	i = bis.read(buffer);
                }
        	}catch(Exception e){
        		e.printStackTrace();
        	}finally{
        		if(bis!=null)
        			try{bis.close();}catch(Exception e){}	
        		if(fis!=null)
        			try{fis.close();}catch(Exception e){}
        	}
        }
	}
    
	
}
