package action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.mybatis.spring.submitted.xa.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import entity.ClassName;
import entity.FileAll;
import entity.Subject;
import entity.User;
import service.FileAllService;
import service.FileService;
import service.SubjectService;

@Controller
public class FileController {
	@Autowired
	FileService fileservice;
	@Autowired
	FileAllService fileallservice;
	@Autowired
	SubjectService subjectService;

	@RequestMapping("/ItemList.do")
	public ModelAndView classList() {
		Map<String, Object> model = new HashMap<String, Object>();
		List<ClassName> files = fileservice.className();
		model.put("files", files);
		return new ModelAndView("Home", model);
	}
	@RequestMapping("/search.do")
	public ModelAndView search(String fileName) {
		Map<String, Object> model = new HashMap<String, Object>();
		System.out.println("filename++++++"+fileName);
		List<FileAll> filealls = fileallservice.search(fileName);
		model.put("filealls", filealls);
		return new ModelAndView("search", model);
	}

	@RequestMapping("/showFileAll.do")
	public ModelAndView showfileAll(int classid, int typeid) {
		System.out.println("show:::::classid" + classid);
		System.out.println("show:::::typeid" + typeid);
		Map<String, Object> model = new HashMap<String, Object>();
		List<FileAll> filealls = fileallservice.getFile(classid, typeid);
		model.put("filealls", filealls);
		model.put("classid", classid);
		model.put("typeid", typeid);
		return new ModelAndView("uploadForm", model);
	}
	
	// @RequestMapping("/upLoadFile.do")
	// public String upload(@RequestParam(value = "file") MultipartFile file,
	// HttpServletRequest request) {
	// ServletContext application = request.getServletContext();
	// String realPath = application.getRealPath("/file");
	// // 为了避免多次上传同一个文件导致命名重复，在文件名前加UUID前缀
	// String prefix = UUID.randomUUID().toString();
	// prefix = prefix.replace("-", "");
	// String fileName = realPath+File.separator+prefix + "_" +
	// file.getOriginalFilename();
	//
	// // int index = file.getOriginalFilename().lastIndexOf(".");
	// // String suffix = file.getOriginalFilename().substring(index+1);
	// // String fileName = realPath+File.separator+"."+suffix;
	// System.out.println(fileName);
	// try {
	// file.transferTo(new File(fileName));
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// return "uploadForm";
	// }
	@RequestMapping("/uploadForm.do")
	public String showFile() {
		return "redirect:upLoadFile.do";
	}
	@RequestMapping("/upLoadFile.do")
	public ModelAndView testUpload(HttpServletRequest request, String author, int classid, int typeid,
			@RequestParam("photo") MultipartFile file) throws Exception {
		ServletContext servletContext = request.getServletContext();
		// 获取服务器下的upload目录
		String realPath = servletContext.getRealPath("/upload");
		File filePath = new File(realPath);
		// 如果目录不存在，则创建该目录
		if (!filePath.exists()) {
			filePath.mkdir();
		}
		OutputStream out;
		InputStream in;
		// 防止重命名uuid_name.jpg
		String prefix = UUID.randomUUID().toString();
		prefix = prefix.replace("-", "");
		String fileName = prefix + "_" + file.getOriginalFilename();
		out = new FileOutputStream(new File(realPath + "\\" + fileName));
		in = file.getInputStream();
		String filepath = realPath + "\\" + fileName;
		System.out.println(filepath);
		FileAll fileall = new FileAll();
		fileall.setAuthor(author);
		System.out.println("author" + author);
		fileall.setClassid(classid);
		System.out.println("classid" + classid);
		fileall.setTypeid(typeid);
		System.out.println("typeid" + typeid);
		Date d = new Date();
		System.out.println(d);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateNowStr = sdf.format(d);
		System.out.println("格式化后的日期：" + dateNowStr);
		fileall.setPubdate(dateNowStr);
		fileall.setFilePath(filepath);
		String fileName2 = fileName.substring(fileName.lastIndexOf("_") + 1);
		fileall.setFileName(fileName2);
		boolean ok = fileallservice.addFile(fileall);
		Map<String, Object> model = new HashMap<String, Object>();
		List<FileAll> filealls = fileallservice.getFile(classid, typeid);
		model.put("filealls", filealls);
		model.put("classid", classid);
		model.put("typeid", typeid);
		byte[] b = new byte[1024];
		int c = 0;
		while ((c = in.read(b)) != -1) {
			out.write(b, 0, c);
			out.flush();
		}
		out.close();
		in.close();
		if (ok) {
			return new ModelAndView("uploadForm", model);
		} else {
			return new ModelAndView("Error", model);
		}
	}
	// 用ResponseEntity<byte[]> 返回值完成文件下载
	@RequestMapping("/fileDownload.do")
	public ResponseEntity<byte[]> fileDownload(HttpServletRequest request, int itemid)
			throws Exception {
		FileAll fil=fileallservice.getdown(itemid);
		
		String path=fil.getFilePath();
		System.out.println("数据库得到的文件地址："+path);
		
		
		byte[] body = null;
		// ServletContext servletContext = request.getServletContext();
		String fileName = path.substring(path.lastIndexOf("_") + 1); // 从uuid_name.jpg中截取文件名
		// String path = servletContext.getRealPath("/WEB-INF/res/" + fileName);
		File file = new File(path);
		InputStream in = new FileInputStream(file);
		body = new byte[in.available()];
		in.read(body);
		HttpHeaders headers = new HttpHeaders();
		fileName = new String(fileName.getBytes("gbk"), "iso8859-1");
		headers.add("Content-Disposition", "attachment;filename=" + fileName);
		HttpStatus statusCode = HttpStatus.OK;
		ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(body, headers, statusCode);
		in.close();
		return response;
	}
	@RequestMapping("/showFile.do")
	public String showFile(HttpServletRequest request, Model m) {
		ServletContext servletContext = request.getServletContext();
		String path = servletContext.getRealPath("/upload");
		System.out.println(path);
		File[] fileList = new File(path).listFiles();
		m.addAttribute("fileList", fileList);
		return "uploadForm";
	}

	@RequestMapping("/itemDelete.do")
	public ModelAndView itemDelete(String itemid, int classid, int typeid) {
		System.out.println(itemid);
		boolean ok = fileallservice.delete(itemid);

		Map<String, Object> model = new HashMap<String, Object>();
		List<FileAll> filealls = fileallservice.getFile(classid, typeid);
		model.put("filealls", filealls);
		model.put("classid", classid);
		if (ok) {
			return new ModelAndView("uploadForm", model);
		} else {
			return new ModelAndView("Error", model);
		}

	}
	

}