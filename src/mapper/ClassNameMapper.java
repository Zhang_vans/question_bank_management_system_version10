package mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import entity.ClassName;

@Repository

public interface ClassNameMapper {
	public List<ClassName> className();

	public int addclass(ClassName className);

	public int delete (int classid);

	
}
