package mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import entity.FileAll;
import entity.User;
import entity.ClassName;
@Repository
public interface FileAllMapper {
	public List<FileAll> getFile(@Param("classid")int classid, @Param("typeid")int typeid);
	public int addFile(FileAll fileall);
	public int delete(String itemid);
	public List<FileAll> search(@Param("fileName")String fileName);
	public FileAll getdown(@Param("itemid")int itemid);
}