package mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import entity.User;

public interface UserMapper {
	// 根据用户名和密码查找
	public User findByUserNameAndPassword(@Param("userid") String userid, @Param("userpass") String userpass);

	// 增加用户
	public void addUser(User user);

	// 根据用户名查询
	public User findByUserName(String username);

	public List<User> getUsers();

	public int delete(String userid);

	public int updateUser(User user);

	public User findByUserid(String userid);

	public int updatePass(@Param("userid") String userid, @Param("userpass") String userpass);

}
