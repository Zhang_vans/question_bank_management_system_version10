package mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import entity.Subject;

public interface SubjectMapper {

	List<Subject> getSubject(int classid);

	public int addtype(Subject subject);

	List<Subject> subjectName(@Param("classid")int classid, @Param("typeid")int typeid);

	int delete(String subjectName);

	int delete(@Param("classid")int classid, @Param("typeid")int typeid);

}
